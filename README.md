# HBase Tools #

Simple utilities for Apache Hbase

* Operations: 
     * Create/Delete/Truncate table(s)
     * Scan table(s)
     * Export data to file
     * Netcat a file (not an HBase operation but useful)
* __Note:__ Tested with HBase 0.94  
[George Hantzaras](https://bitbucket.org/hantzo)