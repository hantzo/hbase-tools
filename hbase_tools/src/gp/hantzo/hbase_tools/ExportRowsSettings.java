package gp.hantzo.hbase_tools;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import gp.hantzo.hbase_tools.core.ISettings;
import gp.hantzo.hbase_tools.core.SettingsSkeleton;
import gp.hantzo.hbase_tools.utils.HBaseEnvs;

@Parameters(separators = "=")
public class ExportRowsSettings implements ISettings {

    @Parameter(names = "-qualTypes", description = "comma separated pairs with values => qualifier:Type \n\tExample: qual1:String,qual2:int,qual3:long \n"
            + "\tAvailable types: All primitives, String, Date\n"
            + "\tEvery other type will be returned as a byte[]", required = true)
    private String qualifierTypes = null;

    @Parameter(names = "-startKey", description = "Start row key", required = true)
    private String startRowKey = null;

    @Parameter(names = "-stopKey", description = "End row key")
    private String stopRowKey = null;

    @Parameter(names = "-file", description = "Export results to file using this value as suffix. Default suffix is current date.")
    private String file = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()).toString();

    @Parameter(names = "-stdout", description = "Export results to STDOUT", arity = 1)
    private boolean useStdout = false;

    @Parameter(names = "-exportRowKey", description = "Export row key as first column")
    private String exportRowKey = null;

    @Parameter(names = "-exportHeaders", description = "Export headers in first line", arity = 1)
    private boolean exportHeaders = true;

    @Parameter(names = "-exportDir", description = "Export dirctory. Must be full path and and must start with '/' or './' and ends with '/'")
    private String exportDir = null;

    @Parameter(names = "-linesMax", description = "Max lines (not accurate)")
    private int linesMax = 0;

    @Parameter(names = "-outDelimeter", description = "Output delimeter")
    private String outDelimeter = ",";

    @ParametersDelegate
    private SettingsSkeleton delegate = new SettingsSkeleton();

    public String getStartRowKey() {
        return startRowKey;
    }

    public String getStopRowKey() {
        return stopRowKey;
    }

    public String getQualifierTypes() {
        return qualifierTypes;
    }

    public int getLinesMax() {
        return linesMax;
    }

    public String getFile() {
        return file;
    }

    public String getOutDelimeter() {
        return outDelimeter;
    }

    public boolean isUseStdout() {
        return useStdout;
    }

    public boolean isExportHeaders() {
        return exportHeaders;
    }

    public String getExportDir() {
        return exportDir;
    }

    public String getExportRowKey() {
        return exportRowKey;
    }

    @Override
    public boolean isHelp() {
        return delegate.isHelp();
    }

    public String getTableName() {
        return delegate.getTableName();
    }

    public String getCf() {
        return delegate.getCf();
    }

    public HBaseEnvs getHbaseEnv() {
        return delegate.getHbaseEnv();
    }

    public boolean isHbaseDistributed() {
        return delegate.isHbaseDistributed();
    }

    public String getHbaseZkQuorum() {
        return delegate.getHbaseZkQuorum();
    }

    public int getBatchSize() {
        return delegate.getBatchSize();
    }

    @Override
    public String toString() {
        return "ExportRowsSettings [qualifierTypes=" + qualifierTypes + ", startRowKey=" + startRowKey + ", stopRowKey="
                + stopRowKey + ", file=" + file + ", useStdout=" + useStdout + ", exportRowKey=" + exportRowKey
                + ", exportHeaders=" + exportHeaders + ", linesMax=" + linesMax + ", exportDir=" + exportDir
                + ", outDelimeter=" + outDelimeter + ", delegate=" + delegate + "]";
    }

}
