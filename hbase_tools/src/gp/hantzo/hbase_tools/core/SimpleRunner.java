package gp.hantzo.hbase_tools.core;

import org.apache.log4j.Logger;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

/**
 * Simple JCommander Runner.
 *
 *
 */
public class SimpleRunner {

    private static final Logger log = Logger.getLogger(SimpleRunner.class);

    public static boolean startRunner(String name, ISettings settings, String[] args) {
        JCommander jc = null;
        try {
            jc = new JCommander(settings);
            jc.parse(args);
            jc.setProgramName(name);
            if (settings.isHelp()) {
                jc.usage();
                return false;
            }
        } catch (ParameterException e) {
            log.error("", e);
            jc.usage();
            return false;
        }

        return true;
    }

}
