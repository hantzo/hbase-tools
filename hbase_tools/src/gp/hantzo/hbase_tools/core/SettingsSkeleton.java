package gp.hantzo.hbase_tools.core;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import gp.hantzo.hbase_tools.utils.HBaseEnvs;

/**
 * Base settings for our JCommander runners.
 *
 */
@Parameters(separators = "=")
public class SettingsSkeleton implements ISettings {

    @Parameter(names = "--help", help = true)
    private boolean help = false;

    @Parameter(names = "-table", description = "Table (and column family) name", required = true)
    private String tableName = null;

    @Parameter(names = "-cf", description = "If you want a different column family set this one")
    private String cf = null;

    @Parameter(names = "-batchSize", description = "Batch size")
    private int batchSize = 100;

    @Parameter(names = "-hbase", description = "HBase environment: LOCAL, PROD")
    private HBaseEnvs hbaseEnv = HBaseEnvs.LOCAL;

    @Parameter(names = "-hbaseDist", description = "HBase distributed mode", arity = 1)
    private boolean hbaseDistributed = false;

    @Parameter(names = "-hbaseZqQuorum", description = "HBase ZK Quorum")
    private String hbaseZkQuorum;

    @Override
    public boolean isHelp() {
        return help;
    }

    public String getTableName() {
        return tableName;
    }

    public String getCf() {
        return cf;
    }

    public HBaseEnvs getHbaseEnv() {
        return hbaseEnv;
    }

    public boolean isHbaseDistributed() {
        return hbaseDistributed;
    }

    public String getHbaseZkQuorum() {
        return hbaseZkQuorum;
    }

    public int getBatchSize() {
        return batchSize;
    }

    @Override
    public String toString() {
        return "SettingsSkeleton [help=" + help + ", tableName=" + tableName + ", cf=" + cf + ", batchSize=" + batchSize
                + ", hbaseEnv=" + hbaseEnv + ", hbaseDistributed=" + hbaseDistributed + ", hbaseZkQuorum=" + hbaseZkQuorum
                + "]";
    }
}
