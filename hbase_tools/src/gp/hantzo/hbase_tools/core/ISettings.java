package gp.hantzo.hbase_tools.core;

/**
 * Base interface for our JCommander settings/parameters.
 *
 *
 */
public interface ISettings {

    boolean isHelp();
}
