package gp.hantzo.hbase_tools;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotEnabledException;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.util.Bytes;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

/**
 *
 * This class is used to create, delete, or truncate a specified hbase table. The default action is truncate (if you don't
 * specify anything else)
 *
 */
@Parameters(separators = "=")
public class SingleTableActions {

    @Parameter(names = "-table", description = "The table to be handled", required = true)
    private static String tableName = null;

    @Parameter(names = { "-c", "-create" }, description = "Should be set to true if you only want to create the specified tables.")
    private static String onlyCreateTables = "false";

    @Parameter(names = { "-delete", "-d" }, description = "Should be set to true if you only want to delete the specified tables.")
    private static String onlyDeleteTables = "false";

    @Parameter(names = "-splits", description = "Total initial regions per table.")
    private static int splits = 8;

    @Parameter(names = { "-coprocessor", "-cpr" }, description = "The coprocessor's (if any) name. "
            + "Example name: com.intracom.cep.hbase.TestCoprocessor")
    private static String coprocessor = null;

    @Parameter(names = "-lab", description = "Set this to true if you want to use lab's hbase. Else, localhost hbase is used")
    private static String lab = "false";

    @Parameter(description = ", or type any key to display usage")
    private static List<String> help;

    private static SingleTableActions javaShell = new SingleTableActions();
    private static JCommander jc = new JCommander(javaShell);

    public static JCommander getJc() {
        return jc;
    }

    private static Configuration conf;
    private static Admin hbase = null;
    private static Connection connection;

    /**
     * @param args
     */
    public static void main(String[] args) {

        byte[][] splitKeys = null;
        resetFields();
        conf = HBaseConfiguration.create();

        try {
            jc.setProgramName("SingleTableActions");
            jc.parse(args);
            if (help != null) {
                jc.usage();
                System.exit(0);
            }
            if (!Boolean.parseBoolean(lab)) {
                conf.set("hbase.rootdir", "file://${hbase.tmp.dir}/hbase");
                conf.setBoolean("hbase.cluster.distributed", false);
                conf.set("hbase.zookeeper.quorum", "localhost");
            }
            try {
                connection = ConnectionFactory.createConnection(conf);
                hbase = connection.getAdmin();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (ParameterException e) {
            jc.usage();
            System.err.println(e.getMessage() + "\n");
            javaShell = new SingleTableActions();
            jc = new JCommander(javaShell);
            return;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        splitKeys = new byte[splits - 1][];
        int padding = Integer.toString(splits).length();
        for (int j = 0; j < (splits - 1); j++) {
            splitKeys[j] = Bytes.toBytes(String.format("%" + padding + "s", ("" + (j + 2)).replace(' ', '0')));
        }
        boolean exception = false;
        while (tableName != null) {
            try {
                if (!Boolean.parseBoolean(onlyCreateTables) && !exception) {
                    hbase.disableTable(TableName.valueOf(tableName));
                    hbase.deleteTable(TableName.valueOf(tableName));
                    System.out.println("Deleted table: " + tableName);
                }
                if (!Boolean.parseBoolean(onlyDeleteTables) || exception) {
                    if (!hbase.tableExists(TableName.valueOf(tableName))) {
                        if (coprocessor != null) {
                            HBaseTruncateTables.create(tableName, tableName, splitKeys, coprocessor, hbase);
                        } else {
                            HBaseTruncateTables.create(tableName, tableName, splitKeys, null, hbase);
                        }
                    } else {
                        System.out.println("Table: " + tableName + " already exists. In order to alter its regions, "
                                + "you have to truncate it.");
                    }
                }
                if (!exception) {
                    break;
                } else {
                    exception = false;
                }
            } catch (TableNotFoundException tnfe) {
                if (!Boolean.parseBoolean(onlyDeleteTables)) {
                    exception = true;
                } else {
                    System.out.println("Table: '" + tableName + "' does not exist anyway.");
                    break;
                }
            } catch (TableNotEnabledException tnee) {
                try {
                    hbase.enableTable(TableName.valueOf(HBaseTruncateTables.getTableFromException(tnee)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        javaShell = new SingleTableActions();
        jc = new JCommander(javaShell);
    }

    private static void resetFields() {
        tableName = null;
        onlyCreateTables = "false";
        onlyDeleteTables = "false";
        splits = 8;
        coprocessor = null;
        lab = "false";
        System.out.println("Defaults were: \ntableName = " + tableName + "\nonlyCreateTables:" + onlyCreateTables
                + "\nonlyDeleteTables:" + onlyDeleteTables + "\nsplits:" + splits + "\ncoprocessor:" + coprocessor
                + "\nlab:" + lab);
    }

}
