package gp.hantzo.hbase_tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import gp.hantzo.hbase_tools.utils.HBaseUtils;

/**
 *
 * This class is used to scan the specified table using a specified prefix. The prefix is passed in its hexadecimal string
 * representation (shard + agent + hexparallelism in 2 digits). You have to set one of the following set of parameters:<br>
 * 1. tableName, prefix<br>
 * 2. tableName, shard, agent, parallelism index (hexString)<br>
 *
 */
@Parameters(separators = "=")
public class PrefixScan {

    @Parameter(names = "-table", description = "Table (and column family) name", required = true)
    private static String tableName = "test";

    @Parameter(names = "-qualTypes", description = "comma separated pairs with values => qualifier:Type. Example: "
            + "qual1:String,qual2:int,qual3:long \n" + "\tAvailable types: " + "\n\tAll primitives, String, Date\n"
            + "\tEvery other type will be returned as a byte[]")
    private static String qualifierTypes = "";// "outboundCallID:String,callingNumber:int,startTime:date,waitingTime:long";

    @Parameter(names = "-cf", description = "If you want a different column family set this one.")
    private static String cf = null;

    @Parameter(names = "-batch", description = "")
    private static int resultSize = 50;

    @Parameter(names = "-pHex", description = "prefix")
    private static String prefixHex = null;

    @Parameter(names = "-shard", description = "HBase shard")
    private static String shard = null;

    @Parameter(names = "-agent", description = "Flume agent name")
    private static String agent = null;

    @Parameter(names = "-parallelismHex", description = "Hexadecimal string representation with 2 digits")
    private static String parallelismHex = null;

    @Parameter(names = "-lab", description = "Set this to true if you want to use lab's hbase. Else, localhost hbase is used")
    private static String lab = "false";

    @Parameter(description = ", or type any key to display usage")
    private static List<String> help;

    private static PrefixScan javaShell = new PrefixScan();
    private static JCommander jc = new JCommander(javaShell);

    public static JCommander getJc() {
        return jc;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        resetFields();
        try {
            jc.setProgramName("PrefixScan");
            jc.parse(args);
            if (help != null) {
                jc.usage();
                System.exit(0);
            }
            if ((prefixHex == null)
                    && (((shard == null) && ((agent != null) || (parallelismHex != null)))
                            || ((agent == null) && ((shard != null) || (parallelismHex != null)))
                            || ((parallelismHex == null) && ((shard != null) || (agent != null))) || (((shard == null) && (agent == null)) || (parallelismHex == null)))) {
                throw new ParameterException("");
            }
        } catch (ParameterException e) {
            jc.usage();
            System.err.println(e.getMessage() + "\n");
            return;
        }

        try {
            byte[] colfam = cf != null ? Bytes.toBytes(cf) : Bytes.toBytes(tableName);
            byte[] table = Bytes.toBytes(tableName);
            String[] arguments = qualifierTypes.equals("") ? new String[0] : qualifierTypes.split(",");
            String finalPrefix = prefixHex != null ? prefixHex : (shard + agent + parallelismHex);

            byte[] startRowKey = Bytes.toBytes(finalPrefix
                    + String.format("%16s", Long.toHexString(0)).replace(' ', '0'));

            byte[] lastRowKey = Bytes.toBytes(finalPrefix
                    + String.format("%16s", Long.toHexString(Long.MAX_VALUE)).replace(' ', '0'));

            Map<String, String> qualifiersTypes = new HashMap<String, String>();

            for (String arg : arguments) {
                qualifiersTypes.put(arg.split(":")[0], arg.split(":")[1]);
            }

            Long lastReverseCount = 0l;
            int count = 0;
            while (true) {
                System.out.println("Will scan: " + Bytes.toString(table) + ", from:" + Bytes.toString(startRowKey)
                        + " to: " + Bytes.toString(lastRowKey));
                List<Map<String, Object>> finalResult = scan(table, colfam, qualifiersTypes, finalPrefix, startRowKey,
                        lastRowKey, resultSize, Boolean.parseBoolean(lab));
                if (finalResult.isEmpty() || (lastReverseCount == (Long.MAX_VALUE - 2))) {
                    System.out.println(count > 0 ? "No more results." : "No results");
                    break;
                }
                String resultStr = "";
                for (Map<String, Object> row : finalResult) {
                    lastReverseCount = (Long) row.remove("reverseCount");
                    resultStr += "row:" + row.remove("prefix") + "|" + lastReverseCount + "|" + row.remove("realKey")
                            + " => ";
                    for (String column : row.keySet()) {
                        if (!column.equalsIgnoreCase("rowkeybytes")) {
                            resultStr += (column + " : " + row.get(column)) + ", ";
                        }
                    }
                    System.out.println(resultStr.substring(0, resultStr.length() - 2));
                    resultStr = "";
                }
                System.out.print("Total rows fetched: " + finalResult.size() + "\n"
                        + "Press 1 for more results or 2 to exit: ");
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                if (Integer.parseInt(in.readLine()) == 2) {
                    break;
                } else {
                    Map<String, Object> lastRow = finalResult.get(finalResult.size() - 1);
                    lastReverseCount = HBaseUtils.getReverseCount((byte[]) lastRow.get("rowkeyBytes"), finalPrefix);
                    startRowKey = (byte[]) lastRow.get("rowkeyBytes");
                }
                count++;
            }
            javaShell = new PrefixScan();
            jc = new JCommander(javaShell);
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public static List<Map<String, Object>> scan(byte[] table, byte[] colfam, Map<String, String> qualifiersTypes,
            String prefix, byte[] startRowKey, byte[] lastRowKey, int batchSize, boolean lab) throws IOException {

        Result[] rr = HBaseUtils.getScannerResult(table, colfam, qualifiersTypes.keySet(), startRowKey, lastRowKey, batchSize,
                lab);
        List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();

        byte[] rowkey = null;
        for (int i = 0; i < rr.length; i++) {
            Map<String, Object> myResult = new HashMap<String, Object>();
            rowkey = rr[i].getRow();

            String realkeyStr = HBaseUtils.getRealKey(rowkey, prefix);
            long reverseCount = HBaseUtils.getReverseCount(rowkey, prefix);
            myResult.put("realKey", realkeyStr);
            myResult.put("reverseCount", new Long(reverseCount));
            myResult.put("prefix", prefix);
            myResult.put("rowkeyBytes", rowkey);
            myResult = HBaseUtils.mapResultsSimple(qualifiersTypes, rr[i].getFamilyMap(colfam), myResult);
            res.add(myResult);
        }

        return res;
    }

    private static void resetFields() {
        tableName = "test";

        qualifierTypes = "";// "outboundCallID:String,callingNumber:int,startTime:date,waitingTime:long";

        cf = null;

        resultSize = 50;

        prefixHex = null;

        shard = null;

        agent = null;

        parallelismHex = null;

        lab = "false";

        System.out.println("Defaults\ntableName:" + tableName + "\ncf:" + cf + "\ndshard:" + shard + "\nagent:" + agent
                + "\nparallelismHex:" + parallelismHex + "\nresultSize:" + resultSize + "\nprefixHex:" + prefixHex
                + "\nlab:" + lab + "\nqualifierTypes:" + qualifierTypes);
    }

}
