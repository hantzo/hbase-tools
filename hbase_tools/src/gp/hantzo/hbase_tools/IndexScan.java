package gp.hantzo.hbase_tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.util.Bytes;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import gp.hantzo.hbase_tools.utils.HBaseUtils;

/**
 *
 * This class is used to scan the specified table using a start and stop rowkey. If you don't set a stopRow, the result will be
 * the same as shell's command:<br>
 * scan 'table', {STARTROW => 'startrow', LIMIT => batch} <br>
 * <br>
 * It cannot be guaranteed though, that the decoding will be correct if the prefix is different. You have to also set which is
 * the prefix in the keys. It is assumed that both keys have the same prefix.
 *
 */
@Parameters(separators = "=")
public class IndexScan {

    @Parameter(names = "-table", description = "Table (and column family) name", required = true)
    private static String aTableName = "test";

    @Parameter(names = "-cf", description = "If you want a different column family set this one.")
    private static String cf = null;

    @Parameter(names = "-start", description = "", required = true)
    private static String dStartRowKey = "test";

    @Parameter(names = "-stop", description = "")
    private static String lastRowKey = null;

    @Parameter(names = "-batch", description = "Amount of rows to fetch at a time.")
    private static int mResultSize = 50;

    @Parameter(names = "-p", description = "The prefix of the range scanned. It is assumed that you scan for rows in the same prefix", required = true)
    private static String prefix = "test";

    @Parameter(names = "-qualTypes", description = "comma separated pairs with values => qualifier:Type. Example: "
            + "qual1:String,qual2:int,qual3:long \n" + "\tAvailable types: " + "\n\tAll primitives, String, Date\n"
            + "\tEvery other type will be returned as a byte[]")
    private static String qualifierTypes = "outboundCallID:String,callingNumber:int,startTime:date,waitingTime:long";

    @Parameter(names = "-lab", description = "Set this to true if you want to use lab's hbase. Else, localhost hbase is used")
    private static String lab = "false";

    @Parameter(description = ", or type any key to display usage")
    private static List<String> help;

    private static IndexScan javaShell = new IndexScan();
    private static JCommander jc = new JCommander(javaShell);

    public static JCommander getJc() {
        return jc;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        resetFields();
        try {
            jc.setProgramName("IndexScan");
            jc.parse(args);
            if (help != null) {
                jc.usage();
                System.exit(0);
            }
        } catch (ParameterException e) {
            jc.usage();
            System.err.println(e.getMessage() + "\n");
            return;
        }

        try {
            byte[] colfam;
            if ((cf != null) && (cf != aTableName)) {
                colfam = Bytes.toBytes(cf);
            } else {
                colfam = Bytes.toBytes(aTableName);
            }
            String[] arguments = qualifierTypes.split(",");
            Map<String, String> qualifiersTypes = new HashMap<String, String>();

            for (String arg : arguments) {
                qualifiersTypes.put(arg.split(":")[0], arg.split(":")[1]);
            }
            byte[] startRowKey = Bytes.toBytes(dStartRowKey);
            byte[] lastRowK = lastRowKey != null ? Bytes.toBytes(lastRowKey) : null;
            Long lastReverseCount = 0l;
            int count = 0;
            while (true) {
                System.out.println("Will scan: " + aTableName + ", from:" + Bytes.toString(startRowKey) + " to: "
                        + Bytes.toString(lastRowK));
                List<Map<String, Object>> finalResult = PrefixScan.scan(Bytes.toBytes(aTableName), colfam,
                        qualifiersTypes, prefix, startRowKey, lastRowK, mResultSize, Boolean.parseBoolean(lab));
                if (finalResult.isEmpty() || (lastReverseCount == (HBaseUtils.getReverseCount(lastRowK, prefix) - 1))) {
                    System.out.println(count > 0 ? "No more results." : "No results");
                    break;
                }
                String resultStr = "";
                for (Map<String, Object> row : finalResult) {
                    lastReverseCount = (Long) row.remove("reverseCount");
                    resultStr += "row:" + row.remove("prefix") + "|" + lastReverseCount + "|" + row.remove("realKey")
                            + " => ";
                    for (String column : row.keySet()) {
                        if (!column.equalsIgnoreCase("rowkeybytes")) {
                            resultStr += (column + " : " + row.get(column)) + ", ";
                        }
                    }
                    System.out.println(resultStr);
                    resultStr = "";
                }
                System.out.print("Total rows fetched: " + finalResult.size() + "\n"
                        + "Press 1 for more results or 2 to exit: ");
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                if (Integer.parseInt(in.readLine()) == 2) {
                    break;
                } else {
                    Map<String, Object> lastRow = finalResult.get(finalResult.size() - 1);
                    startRowKey = (byte[]) lastRow.get("rowkeyBytes");
                }
                count++;
            }
            javaShell = new IndexScan();
            jc = new JCommander(javaShell);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void resetFields() {
        aTableName = "test";
        cf = null;
        dStartRowKey = "test";
        lastRowKey = "test";
        mResultSize = 50;
        prefix = "test";
        lab = "false";
        qualifierTypes = "outboundCallID:String,callingNumber:int,startTime:date,waitingTime:long";
        System.out.println("Defaults\naTableName:" + aTableName + "\ncf:" + cf + "\ndStartRowKey:" + dStartRowKey
                + "\nlastRowKey:" + lastRowKey + "\nmResultSize:" + mResultSize + "\nprefix:" + prefix + "\nlab:" + lab
                + "\nqualifierTypes:" + qualifierTypes);
    }
}
