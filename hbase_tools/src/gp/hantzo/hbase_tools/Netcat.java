package gp.hantzo.hbase_tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Netcat {

    /**
     * @param args
     */
    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Enter dataFile IP Port: ");
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String s;
            try {
                s = bufferRead.readLine();
                args = s.split(" ");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String ip = args[1];
        String port = args[2];
        String filename = args[0];

        Process p;
        try {
            p = Runtime.getRuntime().exec(
                    new String[] { "bash", "-c",
                            "nc -v -w 2 " + ip + " " + port + " < " + filename + "" });
            BufferedReader processStream = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = processStream.readLine();
            while (line != null) {
                if (!line.trim().equalsIgnoreCase("ok")) {
                    System.out.println(line);
                }
                line = processStream.readLine();
            }
            processStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
