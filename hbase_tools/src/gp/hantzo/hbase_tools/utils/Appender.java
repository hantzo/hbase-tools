package gp.hantzo.hbase_tools.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * Basic appender for File or STDOUT.
 *
 *
 */
public class Appender {
    private static final Logger log = Logger.getLogger(Appender.class);

    private static final String HEADEL_LABEL_DELIMETER = "_";
    public static final String CEP_ROW_KEY_LABEL_INTERNAL = "CEP_ROWKEY_";

    private String delimeter = "";
    private boolean file = false;
    private PrintWriter writer = null;
    private String fileName;
    private String rowKeyLabel;

    public Appender(String delimeter) {
        this.delimeter = delimeter == null ? "" : delimeter;
        log.info("Appender configured to export rows to STDOUT\n");
    }

    public Appender(String delimeter, String tableName, String fileSuffix, String exportDir, String rowKeyLabel)
            throws FileNotFoundException, UnsupportedEncodingException {

        if (delimeter != null) {
            if ("\\t".equals(delimeter)) {
                this.delimeter = "\t";
            } else {
                this.delimeter = delimeter;
            }
        }

        this.rowKeyLabel = rowKeyLabel;

        file = true;

        validateCreateDir(exportDir);

        exportDir = exportDir.endsWith("/") ? exportDir.substring(0, exportDir.length() - 1) : exportDir;

        fileName = exportDir + "/export_" + tableName + "_" + fileSuffix + ".csv";

        writer = new PrintWriter(fileName, "UTF-8");

        log.info("Appender configured to export rows to file: " + fileName + "\n");
    }

    public void appendHeaders(LinkedHashMap<String, String> qualifiersTypes, String cf) {
        String resultStr = "";
        if ((rowKeyLabel != null) && (rowKeyLabel.length() > 0)) {
            resultStr = cf + HEADEL_LABEL_DELIMETER + rowKeyLabel + delimeter;
        }

        for (String key : qualifiersTypes.keySet()) {
            resultStr += cf + HEADEL_LABEL_DELIMETER + key + delimeter;
        }

        appendReal(resultStr);
    }

    public void append(LinkedHashMap<String, String> qualifiersTypes, List<Map<String, Object>> finalResult) {
        Object val = null;
        if (rowKeyLabel != null) {
            for (Map<String, Object> row : finalResult) {
                val = row.get(CEP_ROW_KEY_LABEL_INTERNAL);
                String resultStr = (val == null ? "" : val.toString()) + delimeter;
                for (Map.Entry<String, String> e : qualifiersTypes.entrySet()) {
                    val = row.get(e.getKey());
                    resultStr += (val == null ? "" : val.toString()) + delimeter;
                }

                appendReal(resultStr);
            }
        } else {
            for (Map<String, Object> row : finalResult) {
                String resultStr = "";
                for (Map.Entry<String, String> e : qualifiersTypes.entrySet()) {
                    val = row.get(e.getKey());
                    resultStr += (val == null ? "" : val.toString()) + delimeter;
                }

                appendReal(resultStr);
            }
        }
    }

    private void validateCreateDir(String dir) {
        if ((dir == null) || (dir.length() < 2) || !(dir.startsWith("/"))) {
            throw new IllegalArgumentException("Invalid dir: " + dir);
        }

        File dirFile = new File(dir);
        if (dirFile.exists()) {
            if (!dirFile.isDirectory() || !dirFile.canWrite()) {
                throw new IllegalStateException("Dir not exists or not writable: " + dir);
            }
        } else {
            if (!dirFile.mkdirs()) {
                throw new IllegalStateException("Cannot create dir: " + dir);
            }
        }
    }

    private void appendReal(String resultStr) {
        if (file) {
            if (resultStr.endsWith(delimeter)) {
                writer.println(resultStr.substring(0, resultStr.lastIndexOf(delimeter)));
            } else {
                writer.println(resultStr);
            }
        } else {
            if (resultStr.endsWith(delimeter)) {
                System.out.println(resultStr.substring(0, resultStr.lastIndexOf(delimeter)));
            } else {
                System.out.println(resultStr);
            }
        }
    }

    public void shutdown() {
        if (writer != null) {
            writer.close();
        }
    }

    @Override
    public String toString() {
        return "Appender [delimeter=" + delimeter + ", file=" + file + ", fileName=" + fileName + ", writer=" + writer + "]";
    }
}
