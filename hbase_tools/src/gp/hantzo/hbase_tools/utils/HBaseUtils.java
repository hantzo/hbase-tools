package gp.hantzo.hbase_tools.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.FilterList.Operator;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;

/**
 * Various HBase utilities.
 *
 *
 */
public class HBaseUtils {

    private static final Logger log = Logger.getLogger(HBaseUtils.class);

    private static final SimpleDateFormat hbaseDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");

    public static Map<String, Object> mapResultsSimple(Map<String, String> qualifiersTypes,
            Map<byte[], byte[]> results, Map<String, Object> myResult) {

        if (qualifiersTypes.keySet().size() > 0) {
            for (byte[] resultColumn : results.keySet()) {
                String currQualifier = Bytes.toString(resultColumn);
                if (qualifiersTypes.get(currQualifier).equalsIgnoreCase("string")) {
                    myResult.put(currQualifier, Bytes.toString(results.get(resultColumn)));
                } else if (qualifiersTypes.get(currQualifier).equalsIgnoreCase("long")) {
                    myResult.put(currQualifier, Bytes.toLong(results.get(resultColumn)));
                } else if (qualifiersTypes.get(currQualifier).equalsIgnoreCase("float")) {
                    myResult.put(currQualifier, Bytes.toFloat(results.get(resultColumn)));
                } else if (qualifiersTypes.get(currQualifier).equalsIgnoreCase("integer")
                        || qualifiersTypes.get(currQualifier).equalsIgnoreCase("int")) {
                    myResult.put(currQualifier, Bytes.toInt(results.get(resultColumn)));
                } else if (qualifiersTypes.get(currQualifier).equalsIgnoreCase("short")) {
                    myResult.put(currQualifier, Bytes.toShort(results.get(resultColumn)));
                } else if (qualifiersTypes.get(currQualifier).equalsIgnoreCase("boolean")) {
                    myResult.put(currQualifier, Bytes.toBoolean(results.get(resultColumn)));
                } else if (qualifiersTypes.get(currQualifier).equalsIgnoreCase("date")) {
                    Date date = null;
                    try {
                        //TODO Add DateFormat per Date
                        date = hbaseDateFormat.parse(Bytes.toString(results.get(resultColumn)));
                    } catch (ParseException e1) {
                        log.warn("", e1);
                    }
                    myResult.put(currQualifier, date);
                } else {
                    log.error("Use raw bytes for Qualifier=" + currQualifier);
                    myResult.put(currQualifier, results.get(resultColumn));
                }
            }
        }
        return myResult;
    }

    public static Result[] getScannerResult(byte[] table, byte[] colfam, Set<String> qualifiers, byte[] startRowKey,
            byte[] lastRowKey, int batchSize, boolean lab) throws IOException {

        HTable hTable;
        Configuration conf = HBaseConfiguration.create();
        if (!lab) {
            conf.set("hbase.rootdir", "file://${hbase.tmp.dir}/hbase");
            conf.setBoolean("hbase.cluster.distributed", false);
            conf.set("hbase.zookeeper.quorum", "localhost");
        }

        hTable = new HTable(conf, table);

        Scan scan = new Scan(startRowKey);
        if (lastRowKey != null) {
            scan.setStopRow(lastRowKey);
        }

        scan.addFamily(colfam);
        scan.setCaching(batchSize);
        if (qualifiers.size() > 0) {
            scan.setBatch(qualifiers.size());
            List<Filter> filters = new ArrayList<Filter>();
            for (String qualifier : qualifiers) {
                filters.add(new QualifierFilter(CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(qualifier))));
            }
            FilterList list = new FilterList(Operator.MUST_PASS_ONE, filters);
            scan.setFilter(list);
        }

        ResultScanner resultScanner = hTable.getScanner(scan);
        Result[] result = resultScanner.next(batchSize);
        resultScanner.close();
        hTable.close();

        return result;
    }

    public static Long getReverseCount(byte[] rowkey, String prefix) {
        String wholeKey = Bytes.toString(rowkey);
        String lastMetaStartCount = wholeKey.substring(prefix.length(), prefix.length() + 16);
        return Long.parseLong(lastMetaStartCount, 16);
    }

    public static String getRealKey(byte[] rowkey, String prefix) {
        String wholeKey = Bytes.toString(rowkey);
        String realkey = wholeKey.substring(prefix.length() + 16);
        return realkey;
    }

    public static Configuration configureHBase(HBaseEnvs hbaseEnv, String hbaseQuorum) throws IOException {

        log.info("Input: hbaseEnv=" + hbaseEnv);
        Configuration conf = HBaseConfiguration.create();
        if (HBaseEnvs.PROD == hbaseEnv) {
            conf.setBoolean("hbase.cluster.distributed", true);
            conf.set("hbase.zookeeper.quorum", "<-----Add here---->");//Add the clusters Zookeeper hosts
        } else {
            //TODO Parse hbaseQuorum if hbaseEnv=null

            log.info("use HBaseEnv LOCAL");
            //conf.set("hbase.rootdir", "file://${hbase.tmp.dir}/hbase");
            conf.setBoolean("hbase.cluster.distributed", false);
            conf.set("hbase.zookeeper.quorum", "localhost");
            conf.setInt("hbase.zookeeper.property.clientPort", 9998);
        }

        return conf;
    }

}
