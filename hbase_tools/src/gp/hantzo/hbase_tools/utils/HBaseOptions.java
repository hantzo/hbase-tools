package gp.hantzo.hbase_tools.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.FilterList.Operator;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;

/**
 * Basic HBase options.
 *
 *
 */
public class HBaseOptions {

    private static final Logger log = Logger.getLogger(HBaseOptions.class);

    private HTable hTable;
    private ResultScanner resultScanner;
    private int batchSize;
    private HBaseEnvs hbaseEnv;

    public HBaseOptions(String tableName, HBaseEnvs hbaseEnv, String hbaseQuorum) throws IOException {
        configureHBase(tableName, hbaseEnv, hbaseQuorum);
    }

    private void configureHBase(String tableName, HBaseEnvs hbaseEnv, String hbaseQuorum) throws IOException {
        shutdown();

        hTable = new HTable(HBaseUtils.configureHBase(hbaseEnv, hbaseQuorum), Bytes.toBytes(tableName));
    }

    public void initScan(byte[] colfam, Set<String> qualifiers, byte[] startRowKey,
            byte[] lastRowKey, int batchSize) throws IOException {

        this.batchSize = batchSize;

        Scan scan = new Scan(startRowKey);
        if (lastRowKey != null) {
            scan.setStopRow(lastRowKey);
        }

        scan.addFamily(colfam);
        scan.setCaching(batchSize);
        if (qualifiers.size() > 0) {
            scan.setBatch(qualifiers.size());
            List<Filter> filters = new ArrayList<Filter>();
            for (String qualifier : qualifiers) {
                filters.add(new QualifierFilter(CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(qualifier))));
            }
            FilterList list = new FilterList(Operator.MUST_PASS_ONE, filters);
            scan.setFilter(list);
        }

        resultScanner = hTable.getScanner(scan);
    }

    public Result[] nextRows() throws IOException {
        return resultScanner.next(batchSize);
    }

    public void shutdown() {
        try {
            if (resultScanner != null) {
                resultScanner.close();
            }
        } catch (Exception e) {
            log.warn("", e);
        }

        try {
            if (hTable != null) {
                hTable.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "HBaseOptions [batchSize=" + batchSize + ", hbaseEnv=" + hbaseEnv + ", hTable=" + hTable + ", resultScanner="
                + resultScanner + "]";
    }
}
