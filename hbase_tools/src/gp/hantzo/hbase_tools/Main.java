package gp.hantzo.hbase_tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {

        while (true) {
            System.out
                    .println("----------------- Scanner -----------------\n"
                            + "\nYou can run any of the following as a standalone process using the following command:\n"
                            + "\n\tjava -cp path_to_jar [corresponding command in parentheses below]\n"
                            + "\n(Enter the number of your selection to see list of options)\n"
                            + "\n1.Scan table in a specified prefix (PrefixScan [options]) \n"
                            + "\n2.Scan table using a start row and a stop row (IndexScan [options])\n"
                            + "\n3.Create, delete or truncate a list of tables from a file (one tablename per line) (HBaseTruncateTables [options])\n"
                            + "\n4.Create, delete or truncate a single table (SingleTableActions [options])\n"
                            + "\n5.Send rows to flume agent (per minute) (MultiMsisdnClient [options])\n"
                            + "\n6.Send file to flume agent through netcat (Netcat [options])\n" + "\n7.Exit\n");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            try {
                Integer selection = Integer.parseInt(in.readLine().trim());
                switch (selection) {
                    case 1: {
                        System.out
                                .println("This selection is used to scan the specified table using a specified prefix "
                                        + ". The prefix is passed in its hexadecimal string representation (shard + agent + hexparallelism in 2 digits)"
                                        + ".\nYou have to set one of the following set of parameters: "
                                        + "\n\t1. tableName, prefix"
                                        + "\n\t2. tableName, shard, agent, parallelism index (hexString) \n");
                        System.out.println("Type the necessary parameters, space-separated");
                        PrefixScan.getJc().usage();
                        System.out.println(PrefixScan.getJc().getObjects());
                        String[] arguments = in.readLine().split(" ");
                        PrefixScan.main(arguments);
                        break;
                    }
                    case 2: {
                        System.out
                                .println("This selection is used to scan the specified table using a start "
                                        + "and stop rowkey. If you don't set a stopRow, the result will be the same as shell's command:"
                                        + "\nscan 'table', {STARTROW => 'startrow', LIMIT => batch} \n"
                                        + "It cannot be guaranteed though, that the decoding will be correct if the prefix is different."
                                        + "You have to also set which is the prefix in the keys. \n*It is assumed that"
                                        + " both keys have the same prefix.\n");
                        System.out.println("Type the necessary parameters, space-separated");
                        IndexScan.getJc().usage();
                        String[] arguments = in.readLine().split(" ");
                        IndexScan.main(arguments);
                        break;
                    }
                    case 3: {
                        System.out.println("This selection is used to truncate a set of hbase tables. "
                                + "You can do one of the following:  \n\t 1. Deal with all the existing "
                                + "tables in hbase. You can truncate or only delete the tables.\n\t 2. "
                                + "Specify a list of tables in a simple text file (one table per line). "
                                + "You can truncate, only delete or only create the tables.\n");
                        System.out.println("Type the necessary parameters, space-separated");
                        HBaseTruncateTables.getJc().usage();
                        String[] arguments = in.readLine().split(" ");
                        HBaseTruncateTables.main(arguments);
                        break;
                    }
                    case 4: {
                        System.out
                                .println("This selection is used to create, delete, or truncate a specified hbase table. "
                                        + "The default action is truncate (if you don't specify anything else)");
                        System.out.println("Type the necessary parameters, space-separated");
                        SingleTableActions.getJc().usage();
                        String[] arguments = in.readLine().split(" ");
                        SingleTableActions.main(arguments);
                        break;
                    }
                    case 6: {
                        System.out.println("This class sends a file of events directly to the flume-agent. \n");
                        System.out.println("\noptions: dataFile agentIP agentPort");
                        String[] arguments = in.readLine().split(" ");
                        Netcat.main(arguments);
                        break;
                    }
                    case 7:
                        break;
                    default:
                        System.out.println("Try again...");
                }
                if (selection == 7) {
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }

}
