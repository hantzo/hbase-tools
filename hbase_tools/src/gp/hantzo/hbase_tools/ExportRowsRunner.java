package gp.hantzo.hbase_tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;

import gp.hantzo.hbase_tools.core.SimpleRunner;
import gp.hantzo.hbase_tools.utils.Appender;
import gp.hantzo.hbase_tools.utils.HBaseOptions;
import gp.hantzo.hbase_tools.utils.HBaseUtils;

/**
 * Export HBase to File or to STDOUT.
 *
 *
 */
public class ExportRowsRunner {

    private static final Logger log = Logger.getLogger(ExportRowsRunner.class);

    public static void main(String[] args) {
        ExportRowsSettings settings = new ExportRowsSettings();
        if (!SimpleRunner.startRunner(ExportRowsRunner.class.getSimpleName(), settings, args)) {
            return;
        }

        Appender appender = null;
        HBaseOptions hbaseOpts = null;
        try {
            log.info("Current configuration: " + settings);

            byte[] colfam;
            if ((settings.getCf() != null) && (settings.getCf() != settings.getTableName())) {
                colfam = Bytes.toBytes(settings.getCf());
            } else {
                colfam = Bytes.toBytes(settings.getTableName());
            }

            String[] mixedQualTypes = settings.getQualifierTypes().split(",");
            LinkedHashMap<String, String> qualifiersTypes = new LinkedHashMap<String, String>();

            for (String mixedArg : mixedQualTypes) {
                String[] qualType = mixedArg.split(":");
                qualifiersTypes.put(qualType[0], qualType[1]);
            }

            byte[] startRowK = Bytes.toBytes(settings.getStartRowKey());
            byte[] stopRowK = settings.getStopRowKey() != null ? Bytes.toBytes(settings.getStopRowKey()) : null;

            if (!settings.isUseStdout()) {
                appender = new Appender(settings.getOutDelimeter(), settings.getTableName(), settings.getFile(),
                        settings.getExportDir(), settings.getExportRowKey());
            } else {
                appender = new Appender(settings.getOutDelimeter());
            }
            log.info("appender configured: " + appender);

            hbaseOpts = new HBaseOptions(settings.getTableName(), settings.getHbaseEnv(), settings.getHbaseZkQuorum());
            hbaseOpts.initScan(colfam, qualifiersTypes.keySet(), startRowK, stopRowK, settings.getBatchSize());
            log.info("hbaseOpts configured: " + hbaseOpts);

            log.info("=== Start processing ===\n");

            if (settings.isExportHeaders()) {
                appender.appendHeaders(qualifiersTypes, settings.getCf() != null ? settings.getCf() : settings.getTableName());
            }
            long globalCounter = 0;
            long maxRowsPermitted = Long.MAX_VALUE - (settings.getBatchSize() * 2);
            while (true) {
                Result[] rawResults = hbaseOpts.nextRows();
                if ((rawResults == null) || (rawResults.length == 0)) {
                    break;
                }

                List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

                byte[] rowkey = null;
                for (int i = 0; i < rawResults.length; i++) {
                    Map<String, Object> rowResult = new HashMap<String, Object>();
                    rowkey = rawResults[i].getRow();

                    if (settings.getExportRowKey() != null) {
                        rowResult.put(Appender.CEP_ROW_KEY_LABEL_INTERNAL, Bytes.toString(rowkey));
                    }
                    rowResult = HBaseUtils.mapResultsSimple(qualifiersTypes, rawResults[i].getFamilyMap(colfam), rowResult);
                    result.add(rowResult);
                }

                if (!result.isEmpty()) {
                    appender.append(qualifiersTypes, result);
                    globalCounter += result.size();

                    if (!settings.isUseStdout()) {
                        log.info("Append " + result.size() + " rows. All written rows=" + globalCounter);
                    }
                }

                if ((settings.getLinesMax() > 0) && (settings.getLinesMax() < globalCounter)) {
                    log.warn("Reach Max lines threashold (currently written lines " + globalCounter
                            + "). Stop export");
                    break;
                }
                if (globalCounter >= maxRowsPermitted) {
                    log.warn("\nAlmost reach Long.MAX_VALUE lines (currently written lines " + globalCounter
                            + "). Stop export");
                    break;
                }
            }

            log.info("=== End processing ===\nFinally export " + globalCounter
                    + " rows. Export completed succesfully!!");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (appender != null) {
                appender.shutdown();
            }
            if (hbaseOpts != null) {
                hbaseOpts.shutdown();
            }
        }
    }
}
