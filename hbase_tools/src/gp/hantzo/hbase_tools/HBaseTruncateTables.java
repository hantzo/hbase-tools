package gp.hantzo.hbase_tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotEnabledException;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.util.Bytes;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

/**
 *
 *
 * This class is used to truncate a set of hbase tables. You can do one of the following: <br>
 * <p>
 * 1. Deal with all the existing tables in hbase. You can truncate or only delete the tables. The table is created exactly as it
 * was before, but with no rows inside.
 * <p>
 * 2. Specify a list of tables in a simple text file (one table per line) and (optionally) the coprocessor's name next to the
 * table (separated by space). You can truncate, only delete or only create the tables. <br>
 *
 */
@Parameters(separators = "=")
public class HBaseTruncateTables {

    @Parameter(names = "-f", description = "Name of the file that contains the tables to be truncated. Should be set if you do not want "
            + "to deal with all existing tables. The file should contain one table name per line and (optionally)"
            + " the coprocessor's name next to it (separated by space).")
    private static String inputFileName = null;

    @Parameter(names = { "-c", "-create" }, description = "Should be set to true if you only want to create the specified tables.")
    private static String onlyCreateTables = "false";

    @Parameter(names = { "-delete", "-d" }, description = "Should be set to true if you only want to delete the specified tables.")
    private static String onlyDeleteTables = "false";

    @Parameter(names = "-all", description = "True if you want to deal with all existing tables. If no input file is set, this one defaults to 'true'.")
    private static String allTables = "true";

    @Parameter(names = "-splits", description = "Total initial regions per table.")
    private static int splits = 8;

    @Parameter(names = { "-cprocessor", "-cpr" }, description = "Set this name if you want to set the same coprocessor to all tables handled."
            + "You should bear in mind that every coprocessor name, set in the text file next to the table name, will be ignored if you do not"
            + "leave this parameter null.")
    private static String coprocessor = null;

    @Parameter(names = "-lab", description = "Set this to true if you want to use lab's hbase. Else, localhost hbase is used")
    private static String lab = "false";

    @Parameter(description = ", or type any key to display usage")
    private static List<String> help;

    private static HBaseTruncateTables javaShell = new HBaseTruncateTables();
    private static JCommander jc = new JCommander(javaShell);

    public static JCommander getJc() {
        return jc;
    }

    private static Configuration conf;
    private static Admin hbase = null;
    private static Connection connection;

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        byte[][] splitKeys = null;
        resetFields();
        conf = HBaseConfiguration.create();

        try {
            jc.setProgramName("HBaseTruncateTables");
            jc.parse(args);
            if (help != null) {
                jc.usage();
                System.exit(0);
            }
            if (!Boolean.parseBoolean(lab)) {
                conf.set("hbase.rootdir", "file://${hbase.tmp.dir}/hbase");
                conf.setBoolean("hbase.cluster.distributed", false);
                conf.set("hbase.zookeeper.quorum", "localhost");
            }

            try {
                connection = ConnectionFactory.createConnection(conf);
                hbase = connection.getAdmin();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (ParameterException e) {
            jc.usage();
            System.err.println(e.getMessage() + "\n");
            javaShell = new HBaseTruncateTables();
            jc = new JCommander(javaShell);
            return;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        Map<String, String> tables = new HashMap<String, String>();
        splitKeys = new byte[splits - 1][];
        int padding = Integer.toString(splits).length();
        for (int j = 0; j < (splits - 1); j++) {
            splitKeys[j] = Bytes.toBytes(String.format("%" + padding + "s", ("" + (j + 2))).replace(' ', '0'));
        }
        if (inputFileName != null) {
            allTables = "false";
            File inputFile = new File(inputFileName);
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
            String line = in.readLine();
            while (line != null) {
                String[] contents = line.split(" ");
                for (int i = 0; i < contents.length; i++) {
                    System.out.println(contents[i] + " ");
                }
                if (contents.length > 1) {
                    tables.put(contents[0].trim(), contents[1].trim());
                } else {
                    tables.put(contents[0].trim(), "null");
                }
                line = in.readLine();
            }
            in.close();
        } else if (Boolean.parseBoolean(allTables)) {
            System.out
                    .println("\n\nWarning: You are about to delete ALL tables in hbase and create them from scratch. Any data in"
                            + " the tables will be lost. Do you wish to continue?y/n");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            String response = in.readLine().trim();
            if (response.equalsIgnoreCase("y") || response.equalsIgnoreCase("yes")) {
                ;
            } else {
                throw new ParameterException("Reset parameters");
            }
            HTableDescriptor[] listOfTables = hbase.listTables();
            for (HTableDescriptor table : listOfTables) {
                if (hbase.isTableEnabled(TableName.valueOf(table.getNameAsString()))) {
                    hbase.disableTable(TableName.valueOf(table.getNameAsString()));
                }
                hbase.deleteTable(TableName.valueOf(table.getNameAsString()));
                if (!Boolean.parseBoolean(onlyDeleteTables)) {
                    hbase.createTable(table, splitKeys);
                }
            }
        } else {
            throw new ParameterException("Reset parameters.Type HBaseTruncateTables (any key) to display usage");
        }

        while (!tables.isEmpty()) {
            String[] tableNames = tables.keySet().toArray(new String[tables.size()]);
            try {
                for (int i = 0; i < tableNames.length; i++) {
                    String tableName = tableNames[i];
                    if (!Boolean.parseBoolean(onlyCreateTables)) {
                        hbase.disableTable(TableName.valueOf(tableName));
                        hbase.deleteTable(TableName.valueOf(tableName));
                        System.out.println("Deleted table: " + tableName);
                    }
                    if (!Boolean.parseBoolean(onlyDeleteTables)) {
                        if (coprocessor != null) {
                            create(tableName, tableName, splitKeys, coprocessor, null);
                        } else if ((tables.get(tableName) == null) || tables.get(tableName).equalsIgnoreCase("null")) {
                            create(tableName, tableName, splitKeys, null, null);
                        } else {
                            create(tableName, tableName, splitKeys, tables.get(tableName), null);
                        }
                    }
                    tables.remove(tableName);
                    tableNames = tables.keySet().toArray(new String[tables.size()]);
                }
            } catch (TableNotFoundException tnfe) {
                String tableName = getTableFromException(tnfe);
                String cpr = null;
                if (coprocessor != null) {
                    cpr = coprocessor;
                } else if (!tables.isEmpty() && (tables.get(tableName) != null)) {
                    cpr = tables.get(tableName);
                }

                if (!Boolean.parseBoolean(onlyDeleteTables)) {
                    create(tableName, tableName, splitKeys, cpr, null);
                } else {
                    System.out.println("Table: " + tableName + " does not exist anyway.");
                }
                tables.remove(tableName);
            } catch (TableNotEnabledException tnee) {
                hbase.enableTable(TableName.valueOf(getTableFromException(tnee)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (TableExistsException tee) {
                tables.remove(getTableFromException(tee));
                tableNames = tables.keySet().toArray(new String[tables.size()]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        javaShell = new HBaseTruncateTables();
        jc = new JCommander(javaShell);

    }

    public static void create(String tableName, String colFam, byte[][] splitKeys, String coprocessor, Admin admin)
            throws IOException {

        HTableDescriptor table = new HTableDescriptor(TableName.valueOf(tableName));
        HColumnDescriptor colFamily = new HColumnDescriptor(colFam);
        table.addFamily(colFamily);
        if (coprocessor != null) {
            table.addCoprocessor(coprocessor);
        }
        if (hbase != null) {
            hbase.createTable(table, splitKeys);
        } else {
            admin.createTable(table, splitKeys);
        }
        System.out.println("Created table: " + tableName);
    }

    public static String getTableFromException(Exception e) {
        String message = e.getMessage();
        System.out.println(message);
        int index = message.lastIndexOf(": ") + (": ".length());
        String tableName = message.substring(index);
        return tableName;
    }

    private static void resetFields() {
        inputFileName = null;
        onlyCreateTables = "false";
        onlyDeleteTables = "false";
        allTables = "true";
        splits = 8;
        coprocessor = null;
        lab = "false";
        System.out.println("inputFileName = " + inputFileName + "\nonlyCreateTables:" + onlyCreateTables
                + "\nonlyDeleteTables:" + onlyDeleteTables + "\nallTables:" + allTables + "\nsplits:" + splits
                + "\ncoprocessor:" + coprocessor + "\nlab:" + lab);
    }
}
